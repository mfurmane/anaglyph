﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunnyCamera : MonoBehaviour {

	public Material anaglyphMat;
	private		RenderTexture 		leftEyeRT;   
	private 	RenderTexture		rightEyeRT;
	public Texture2D tex1;
	public Texture2D tex2;
	private		GameObject	 		leftEye;
	private	GameObject rightEye;
	public Shader shader;
	public Camera acamera;



	// Use this for initialization
	void Start () {
		Camera activeCamera = GetComponent<Camera>();
		activeCamera.SetReplacementShader (shader, null);

		leftEye = new GameObject ("leftEye", typeof(Camera));
		rightEye = new GameObject ("rightEye", typeof(Camera));
		Camera leftCamera = leftEye.GetComponent<Camera> ();
		Camera rightCamera = rightEye.GetComponent<Camera> ();



		leftCamera.CopyFrom (acamera);
		rightCamera.CopyFrom (acamera);

		leftEye.AddComponent<GUILayer> ();
		rightEye.AddComponent<GUILayer> ();

		leftEyeRT = new RenderTexture (Screen.width, Screen.height, 24);
		rightEyeRT = new RenderTexture (Screen.width, Screen.height, 24);


		//Shader.SetGlobalTexture ("_LeftTex", leftEyeRT);
		//Shader.SetGlobalTexture ("_RightTex", rightEyeRT);

		//Shader.SetGlobalTexture ("_MainTex", tex1);
		//Shader.SetGlobalTexture ("_LeftTex", tex1);
		//Shader.SetGlobalTexture ("_RightTex", tex2);
		//Shader.SetGlobalTexture ("Texture1", tex1);
		//Shader.SetGlobalTexture ("Texture2", tex2);
		//Debug.Log (shader.);

		leftCamera.targetTexture = leftEyeRT;
		rightCamera.targetTexture = rightEyeRT;
	//	Camera.main.targetTexture = rightEyeRT;

		anaglyphMat.shader = shader;
		anaglyphMat.SetTexture ("_LeftTex", leftEyeRT);
		anaglyphMat.SetTexture ("_RightTex", rightEyeRT);


		leftCamera.depth = Camera.main.depth - 2;
		rightCamera.depth = Camera.main.depth - 1;

		float siz = 0.3f;
		leftEye.transform.position = acamera.transform.position + acamera.transform.TransformDirection (siz, 0f, 0f);
		rightEye.transform.position = acamera.transform.position + acamera.transform.TransformDirection (-siz, 0f, 0f);

		leftEye.transform.SetParent(acamera.transform);
		rightEye.transform.SetParent(acamera.transform);

		leftEye.transform.localEulerAngles = new Vector3 (0, -0.5f, 0);
		rightEye.transform.localEulerAngles = new Vector3 (0, 0.5f, 0);

	//	Camera.main.cullingMask = 0;
	//	Camera.main.backgroundColor = new Color (0f, 0f, 0f, 0f);
		//Camera.main.clearFlags = CameraClearFlags.Nothing;

	}
	
	// Update is called once per frame
	void Update () {

	}
}
