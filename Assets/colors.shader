﻿Shader "Colour Balanced Anaglyph Parametric" {
Properties {
   _LeftTex ("Texture1", 2D) = "white" {}
   _RightTex ("Texture2", 2D) = "white" {}
   }

SubShader {
   Pass {
      //ZTest Always Cull Off ZWrite Off
      //Fog { Mode off }

      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      #pragma fragmentoption ARB_precision_hint_fastest
      #include "UnityCG.cginc"
      
      sampler2D _LeftTex;
      sampler2D _RightTex;


			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};

      struct v2f {
         float4 pos : POSITION;
         float2 uv : TEXCOORD0;
      };


    
      v2f vert( appdata v )
      {

				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.pos);
				float2 uv =  v.uv;
				o.uv = uv;
				return o;
         //v2f o;
         //o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
         //float2 uv = MultiplyUV( UNITY_MATRIX_TEXTURE0, v.texcoord );
         //o.uv = uv;
         //return o;
      }
      
      half4 frag (v2f i) : COLOR
      {
         float ra, ga, ba, r1, g1, b1, r2, g2, b2;
         float4 texL = tex2D(_LeftTex, i.uv);
         float4 texR = tex2D(_RightTex, i.uv);
       //  float4 texL = float4(0,0,1,1);
       //  float4 texR = float4(0,0,1,1);
         float4 texRGB;

         r1	=	texL.r * 0.299 + texL.g * 0.587 + texL.b * 0.114;
         g1	=	0;
         b1	=	0;
         
         r2	=	0;
         g2	=	texR.r * 0.299 + texR.g * 0.587 + texR.b * 0.114;
         b2	=	texR.r * 0.299 + texR.g * 0.587 + texR.b * 0.114;
        

         texRGB = float4(r1+r2,g1+g2,b1+b2,1);
//         texRGB = float4(1,0.3,0.3,1);
         return texRGB;
      }
      ENDCG
   }
}   
   Fallback off
} 