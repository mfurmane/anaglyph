﻿using UnityEngine;
using System.Collections;

public class halfTransparent : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Material m = GetComponent<MeshRenderer> ().material;
		m.color = new Color (m.color.r, m.color.g, m.color.b, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
