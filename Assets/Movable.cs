﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour {

	private bool move = false;
	private Vector3 opos;
	private Vector3 npos;
	public int speed = 3;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.platform != RuntimePlatform.Android) {
			if (Input.GetMouseButton (0)) {
				//opos = Input.mousePosition = 
				transform.position -= new Vector3 (speed*Input.GetAxis ("Mouse X"), 0, speed*Input.GetAxis ("Mouse Y"));
//				transform.RotateAround (transform.position, Vector3.right, Input.GetAxis ("Mouse Y") * 5);
			}
		}

		if (Input.touches.Length == 1) {
			Touch touch = Input.touches [0];
			if (touch.tapCount == 1) {
				if (!move) {
					move = true;
					opos = touch.position;
				} else {
					npos = touch.position;
					float max = Mathf.Max (Mathf.Max (Mathf.Abs (npos.x - opos.x), Mathf.Abs (npos.y - opos.y)), Mathf.Abs (npos.z - opos.z));
					float m1 = (npos.x - opos.x);
					float m2 = (npos.y - opos.y);
					//Debug.Log (transform.position + " " + m1 + " " + m2);
					transform.RotateAround (transform.position, new Vector3 (m2, -m1, 0), max / 50f);
					if (m1 == max || (max + m1) == 0)
						transform.RotateAround (transform.position, new Vector3 (0, -m1, 0), max / 50f);
					if (m2 == max || (max + m2) == 0)
						transform.RotateAround (transform.position, new Vector3 (m2, 0, 0), max / 50f);
				}
			}
		}
		else {
			move = false;
		}
	}
}
