﻿Shader "Hidden/NewImageEffectShader"
{
	Properties
	{
		_MainTex1 ("Texture", 2D) = "white" {}
		_MainTex2 ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex1;
			sampler2D _MainTex2;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex1, i.uv);
				fixed4 col2 = tex2D(_MainTex2, i.uv);
				// just invert the colors
				//col = 1 - col;
				return col2 - col;
			}
			ENDCG
		}
	}
}
