﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monitoring : MonoBehaviour {

	public float range = 50;
	public float main;
	public bool grow = true;

	// Use this for initialization
	void Start () {
		main = transform.eulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (grow) {
			if (transform.eulerAngles.y < (main + range))
				transform.eulerAngles += new Vector3 (0, 1, 0);
			else
				grow = false;
		} else {
			if (transform.eulerAngles.y > (main - range))
				transform.eulerAngles -= new Vector3 (0, -1, 0);
			else
				grow = true;
		}
	}
}
