﻿using UnityEngine;
using System.Collections;

public class ACamera : MonoBehaviour {

	public float depth;

	//public System.Collections.Generic.List<GameObject> objec = new System.Collections.Generic.List<GameObject>();
	private bool move = false;
	private Vector3 opos;
	private Vector3 npos;
	private Vector2 pos;
	private Vector2 newpos;
	private float ol;
	private float ne;
	private bool scroll = false;
	private Vector3 mousePosition;
	private Vector3 lastMousePosition;
	public Shader shader;

	// Use this for initialization
	void Start () {
		Camera c = GetComponent<Camera> ();
		c.depth = depth;
		Camera activeCamera = GetComponent<Camera>();
		activeCamera.SetReplacementShader (shader, null);
		Debug.Log (shader);
	}
	
	// Update is called once per frame
	void Update () {
		Camera activeCamera = GetComponent<Camera>();
		//RenderTexture texture = activeCamera.targetTexture;

		/*
		Texture2D tex = new Texture2D(activeCamera.pixelWidth, activeCamera.pixelHeight, TextureFormat.RGB24, false);

		// Initialize and render
		RenderTexture rt = new RenderTexture(activeCamera.pixelWidth, activeCamera.pixelHeight, 24);
		activeCamera.targetTexture = rt;
		activeCamera.Render();
		RenderTexture.active = rt;


		// Read pixels
		tex.ReadPixels(new Rect(0, 0, activeCamera.pixelWidth, activeCamera.pixelHeight), 0, 0);
		for (int i = 0; i < activeCamera.pixelWidth; i++) {
			for (int j = 0; j < activeCamera.pixelHeight; j++) {
				tex.SetPixel (i, j, Color.black);
			}
		}

		// Clean up
		activeCamera.targetTexture = null;
		RenderTexture.active = null; // added to avoid errors 
		DestroyImmediate(rt);
*/
		//activeCamera.RenderWithShader(Shader.Find("ColorReplacement"), null);







		if (Input.touches.Length == 1) {
			Touch touch = Input.touches [0];
			if (touch.tapCount == 1) {
				if (!move) {
					move = true;
					pos = touch.position;
				} else {
					if (Mathf.Abs (touch.deltaPosition.x) > Mathf.Abs (touch.deltaPosition.y)) {
						if (pos.x > touch.position.x)
							transform.Translate (Vector3.right);
						if (pos.x < touch.position.x)
							transform.Translate (Vector3.left);
					} else {
						if (pos.y > touch.position.y)
							transform.Translate (Vector3.up);
						if (pos.y < touch.position.y)
							transform.Translate (Vector3.down);
					}
				}
			}
		} else
			move = false;

		/*if (Input.touches.Length == 2) {
			Touch touch1 = Input.touches [0];
			Touch touch2 = Input.touches [1];
			if (!scroll) {
				pos = touch1.position - touch2.position;
				scroll = true;
			} else {
				newpos = touch1.position - touch2.position;
				ol = pos.x * pos.x + pos.y * pos.y;
				ne = newpos.x * newpos.x + newpos.y * newpos.y;
				if (ol > ne) {
					transform.Translate (Vector3.back);
				} else if (ne > ol) {
					transform.Translate (Vector3.forward);
				}
			}
		} else
			scroll = false;
			*/
		
	}
}
